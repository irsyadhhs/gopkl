package irsyadhhs.cs.upi.edu.gopkl3;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;

import static irsyadhhs.cs.upi.edu.gopkl3.AppConfig.SP;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MapsMenu.OnListFragmentInteractionListener {
    //Button btnCari;
    //EditText etname;
    String un;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences sp = getSharedPreferences(SP, MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();

        this.un = sp.getString("un", "deafult");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header=navigationView.getHeaderView(0);
        LinearLayout sideNavLayout = (LinearLayout)header.findViewById(R.id.sideNavLayout);
        sideNavLayout.setBackgroundResource(R.color.primary);

        displaySelectedScreen(R.id.nav_maps);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        EditText etname = (EditText) findViewById(R.id.etmain);
        Button btnCari = (Button) findViewById(R.id.button3);
        //noinspection SimplifiableIfStatement
        if (id == R.id.mSearch) {
            //Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();

            etname.setVisibility(View.VISIBLE);
            btnCari.setVisibility(View.VISIBLE);
            return true;
        }else if (id == R.id.mRefresh) {
            //Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();

            //ListView lv = (ListView) findViewById(R.id.listNearby);

            return true;
        }else if (id == R.id.mLogout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to Logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedPreferences sp = getSharedPreferences(SP, MODE_PRIVATE);
                            SharedPreferences.Editor ed = sp.edit();
                            ed.putString("st", "GAGAL").commit();
                            Intent in = new Intent(MainActivity.this, RegisterAct.class);
                            startActivity(in);
                            MainActivity.this.finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;
        AppCompatActivity appx = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_maps:
                fragment = new MapsMenu();
                break;
            case R.id.nav_profile:
                fragment = new ProfileMenu();
                break;
            case R.id.nav_profileset:
                fragment = new ProfilesetMenu();
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        /*int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action
        } else if (id == R.id.nav_history) {

        } else if (id == R.id.nav_profileset) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);*/

        displaySelectedScreen(item.getItemId());
        return true;
    }

    @Override
    public void onListFragmentInteraction(String d) {
        ProfileMenu pm = (ProfileMenu) getSupportFragmentManager().findFragmentById(R.id.nav_profile);
        if (pm != null) {
            // If article frag is available, we're in two-pane layout...

            // Call a method in the ArticleFragment to update its content
            pm.setListView(d);
        }else{
            Log.d("an", "ok");
        }
    }
}
