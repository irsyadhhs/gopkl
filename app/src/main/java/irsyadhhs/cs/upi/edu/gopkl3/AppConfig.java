package irsyadhhs.cs.upi.edu.gopkl3;

/**
 * Created by HARVI on 5/11/2017.
 */

public class AppConfig {
    public static final String TAG = "go-pkl";
    public static final String SP = "go_pkl.irsyaddhs.cs.upi.edu.go_pkl";
    public static final String API_KEY = "AIzaSyC73TEggymYHQ1imRJ2yEJ7oJW4-mSLbtg";

    public static final String MY_NAME = "myname";
    public static final String MY_EMAIL = "myemail";
    public static final String MY_ID = "myid";
    public static final String APP_AUTH = "appauth";

    public static final String SELECT_PEDAGANG = "http://giftstoreid.xyz/allpedagang.php";
    public static final String SELECT_PEMBELI = "http://giftstoreid.xyz/allpembeli.php";
    public static final String UPDATE_PEMBELI = "http://giftstoreid.xyz/updatepembeli.php";
    public static final String INSERT_PEMBELI = "http://giftstoreid.xyz/insertpembeli.php";
}
