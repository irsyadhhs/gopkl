package irsyadhhs.cs.upi.edu.gopkl3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

import static irsyadhhs.cs.upi.edu.gopkl3.AppConfig.SP;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @BindView(R.id.input_email)
    EditText _emailText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_login)
    Button _loginButton;
    @BindView(R.id.link_signup)
    TextView _signupLink;

    SharedPreferences sp;
    private FirebaseAuth mFirebaseAuth;
    private ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sp = getSharedPreferences(SP, MODE_PRIVATE);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("User");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (mFirebaseAuth.getCurrentUser() != null) {
            onLoginSuccess();
        }

        ButterKnife.bind(this);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                View view = LoginActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(LoginActivity.this, RegisterAct.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");
        progressBar.setVisibility(View.VISIBLE);
        if (!validate()) {
            onLoginFailed();
            return;
        }

       // _loginButton.setEnabled(false);

        String email = _emailText.getText().toString().toLowerCase();
        String password = _passwordText.getText().toString();
        getCurrentUser(email,password);

    }

    public void getCurrentUser(final String email, String password){
        progressBar.setVisibility(View.VISIBLE);
        //authenticate user
        mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {
                            // there was an error
                            Toast.makeText(LoginActivity.this, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
                        } else {
                            FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
                            String UID = currentFirebaseUser.getUid();

                            sp.edit().putString(AppConfig.APP_AUTH, "yes").apply();
                            sp.edit().putString(AppConfig.MY_ID, UID).apply();
                            sp.edit().putString(AppConfig.MY_EMAIL, email).apply();
                            addUserChangeListener(UID);
                            onLoginSuccess();
                        }
                    }
                });
    }

    private void addUserChangeListener(final String UID) {
        // ModelUser data change listener
        mFirebaseDatabase.child(UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                // Check for null
                if (modelUser == null) {
                    return;
                }
                sp.edit().putString(AppConfig.MY_NAME, modelUser.name).apply();
                //setToken(UID);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void onLoginSuccess() {
       // _loginButton.setEnabled(true);
        Intent in = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(in);
        finish();
        //finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString().toLowerCase();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 16) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    /*void checkExist(final String un, final  String pw) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, SELECT_PEMBELI, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "onResponse: playerResult= " + response.toString());
                        String name;
                        String pass;
                        int stop = 0;
                        try {
                            JSONArray jsonArray = response.getJSONArray("users");

                            if (response.getString("success").equalsIgnoreCase("1")) {
                                int i;
                                i = 0;
                                while ((i < jsonArray.length()) && (stop == 0)) {
                                    JSONObject pembeli = jsonArray.getJSONObject(i);
                                    name = pembeli.getString("nama");
                                    pass = pembeli.getString("pass");
                                    if (name.equals(un.toString())) {
                                        stop = 1;
                                        if(pass.equals(pw.toString())){
                                            SharedPreferences sp = getSharedPreferences(SP, MODE_PRIVATE);
                                            SharedPreferences.Editor ed = sp.edit();
                                            ed.putString("un", un);
                                            ed.putString("pw", pw);
                                            ed.putString("st","active");
                                            ed.commit();

                                            Toast.makeText(LoginActivity.this, "Log in successful", Toast.LENGTH_SHORT).show();
                                            Intent in = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(in);
                                            finish();
                                        }else{
                                            Toast.makeText(LoginActivity.this, "Wrong password", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    i++;
                                }

                                if (stop == 0) {
                                    Toast.makeText(LoginActivity.this, "Username doesn't exist", Toast.LENGTH_SHORT).show();
                                }else{

                                }
                            } else if (response.getString("success").equalsIgnoreCase("0")) {

                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                            Log.e(TAG, "parseLocationResult: Error=" + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //menampilkan error pada logcat
                        Log.e(TAG, "onErrorResponse: Error= " + error);
                        Log.e(TAG, "onErrorResponse: Error= " + error.getMessage());

                    }
                }
        );

        AppController.getInstance().addToRequestQueue(request);
    }*/

    /*@Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }*/
}

