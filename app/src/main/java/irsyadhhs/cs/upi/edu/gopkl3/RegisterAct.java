package irsyadhhs.cs.upi.edu.gopkl3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

import static irsyadhhs.cs.upi.edu.gopkl3.AppConfig.SP;

public class RegisterAct extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    @BindView(R.id.input_name)
    EditText _nameText;
    @BindView(R.id.input_address) EditText _addressText;
    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_mobile) EditText _mobileText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @BindView(R.id.btn_signup)
    Button _signupButton;
    @BindView(R.id.link_login)
    TextView _loginLink;
    String lasttime;
    String mUserID, mName, mEmail, mNumber, mAddress, mPassword, mRePassword;
    SharedPreferences sp;
    private FirebaseAuth mFirebaseAuth;
    private ProgressBar progressBar;
    private DatabaseReference mFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sp = getSharedPreferences(SP, MODE_PRIVATE);
        progressBar = findViewById(R.id.progressBar);
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("User");
        mFirebaseAuth = FirebaseAuth.getInstance();
        /*if(sp.getString("st","GAGAL").equals("active")){
            Intent in = new Intent(RegisterAct.this, MainActivity.class);
            startActivity(in);
            finish();
        }*/

        if (mFirebaseAuth.getCurrentUser() != null) {
            onSignupSuccess();
        }
        ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = RegisterAct.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void getText(){
        mName = _nameText.getText().toString().toLowerCase();
        mAddress = _addressText.getText().toString();
        mEmail = _emailText.getText().toString();
        mNumber = _mobileText.getText().toString();
        mPassword = _passwordText.getText().toString();
        mRePassword = _reEnterPasswordText.getText().toString();
    }

    public void signup() {
        Log.d(TAG, "Signup");
        getText();
        if (!validate()) {
            onSignupFailed();
            return;
        }
        registerAuth();
       // _signupButton.setEnabled(false);
    }


    public void onSignupSuccess() {
       // _signupButton.setEnabled(true);
        Intent in = new Intent(RegisterAct.this, LoginActivity.class);
        startActivity(in);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Sign up failed", Toast.LENGTH_LONG).show();
        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        progressBar.setVisibility(View.VISIBLE);
        boolean valid = true;

        if (mName.isEmpty() || mName.length() < 3) {
            _nameText.setError("at least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (mAddress.isEmpty()) {
            _addressText.setError("Enter Valid Address");
            valid = false;
        } else {
            _addressText.setError(null);
        }

       if (mEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (mNumber.isEmpty() || mNumber.length()<10 || mNumber.length()>13) {
            _mobileText.setError("Enter Valid Mobile Number");
            valid = false;
        } else {
            _mobileText.setError(null);
        }

        if (mPassword.isEmpty() || mPassword.length() < 4 || mPassword.length() > 16) {
            _passwordText.setError("between 4 and 16 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (mRePassword.isEmpty() || mRePassword.length() < 4 || mRePassword.length() > 16 || !(mRePassword.equals(mPassword))) {
            _reEnterPasswordText.setError("Password Do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }

    private void createUser(String UID) {
        ModelUser modelUser = new ModelUser(mName, mEmail, mNumber, mAddress);
        //modelUser.setPicture("https://firebasestorage.googleapis.com/v0/b/quixotic-treat-194714.appspot.com/o/defaultpic.png?alt=media&token=d4f871bb-1676-4b41-8f80-11bf12f3f22f");
        mFirebaseDatabase.child(UID).setValue(modelUser);
        addUserChangeListener(UID);
        mFirebaseAuth.signOut();
        if (mFirebaseAuth.getCurrentUser() == null) {
            Intent intent = new Intent(RegisterAct.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /* ModelUser data change listener */
    private void addUserChangeListener(String UID) {
        // ModelUser data change listener
        mFirebaseDatabase.child(UID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ModelUser modelUser = dataSnapshot.getValue(ModelUser.class);
                // Check for null
                if (modelUser == null) {
                    Log.e(TAG, "ModelUser data is null!");
                    return;
                }
                sp.edit().putString(AppConfig.MY_NAME, modelUser.name).apply();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    public void registerAuth(){
        mFirebaseAuth.createUserWithEmailAndPassword(mEmail, mPassword)
                .addOnCompleteListener(RegisterAct.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(RegisterAct.this, "Akun berhasil dibuat", Toast.LENGTH_SHORT).show();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {
                            Toast.makeText(RegisterAct.this, "Email sudah pernah digunakan",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseUser currentFirebaseUser = mFirebaseAuth.getCurrentUser() ;
                            String UID = currentFirebaseUser.getUid();
                            createUser(UID);

                            sp.edit().putString(AppConfig.APP_AUTH, "yes").apply();
                            sp.edit().putString(AppConfig.MY_ID, UID).apply();
                        }
                    }
                });
    }


    /*void checkExist(final String un) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, SELECT_PEMBELI, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "onResponse: playerResult= " + response.toString());
                        String name;
                        int stop = 0;
                        try {
                            JSONArray jsonArray = response.getJSONArray("users");

                            if (response.getString("success").equalsIgnoreCase("1")) {
                                int i;
                                i = 0;
                                while ((i < jsonArray.length()) && (stop == 0)) {
                                    JSONObject pembeli = jsonArray.getJSONObject(i);
                                    name = pembeli.getString("nama");
                                    if (name.equals(un.toString())) {
                                        stop = 1;
                                        Toast.makeText(RegisterAct.this, "Username already exist", Toast.LENGTH_SHORT).show();
                                    }
                                    i++;
                                }

                                if (stop == 0) {
                                    SharedPreferences sp = getSharedPreferences(SP, MODE_PRIVATE);
                                    SharedPreferences.Editor ed = sp.edit();
                                    ed.putString("un", _nameText.getText().toString().toLowerCase());
                                    ed.putString("pw", _passwordText.getText().toString());
                                    ed.putString("st","active");
                                    ed.commit();
                                    tambah();
                                    Toast.makeText(RegisterAct.this, "Register successful", Toast.LENGTH_SHORT).show();
                                    Intent in = new Intent(RegisterAct.this, MainActivity.class);
                                    startActivity(in);
                                    finish();
                                }else{

                                }
                            } else if (response.getString("success").equalsIgnoreCase("0")) {

                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                            Log.e(TAG, "parseLocationResult: Error=" + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //menampilkan error pada logcat
                        Log.e(TAG, "onErrorResponse: Error= " + error);
                        Log.e(TAG, "onErrorResponse: Error= " + error.getMessage());

                    }
                }
        );

        AppController.getInstance().addToRequestQueue(request);
    }


    void tambah() {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM KK:mm a");
        lasttime = sdf.format(now);
        StringRequest postRequest = new StringRequest(Request.Method.POST, INSERT_PEMBELI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // menampilkan respone
                        Log.i("FUCK", response);
                        Log.d("Response POST", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.e(TAG, "onErrorResponse: Error= " + error);
                        Log.e(TAG, "onErrorResponse: Error= " + error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                // Menambahkan parameters post
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", "");
                params.put("nama", _nameText.getText().toString().toLowerCase());
                params.put("pass", _passwordText.getText().toString());
                params.put("lat", "");
                params.put("long", "");
                params.put("req", "");
                params.put("lasttime", lasttime);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(postRequest);
    }*/
}

