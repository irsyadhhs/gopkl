package irsyadhhs.cs.upi.edu.gopkl3;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static irsyadhhs.cs.upi.edu.gopkl3.AppConfig.SP;
import static irsyadhhs.cs.upi.edu.gopkl3.AppConfig.TAG;
import static irsyadhhs.cs.upi.edu.gopkl3.AppConfig.UPDATE_PEMBELI;

/**
 * Created by HARVI on 5/11/2017.
 */

public class ProfilesetMenu extends Fragment {
    EditText etUN;
    EditText etOPW;
    EditText etNPW;
    String un;
    String opw;
    String npw;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        // change R.layout.yourlayoutfilename for each of your fragments

        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_profileset, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Edit Profile");
        etUN = (EditText) getView().findViewById(R.id.etNewUN);
        etOPW = (EditText) getView().findViewById(R.id.etOldPW);
        etNPW = (EditText) getView().findViewById(R.id.etNewPW);
        //sp = getActivity().getSharedPreferences(SP, MODE_PRIVATE);
        //ed = sp.edit();
        Button btnSaved = (Button) getView().findViewById(R.id.btnSave);
        btnSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savechange();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.mSearch).setVisible(false);
        menu.findItem(R.id.mRefresh).setVisible(false);
        menu.findItem(R.id.mLogout).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void savechange() {
        opw = etOPW.getText().toString();
        npw = etNPW.getText().toString();
        etOPW.setText("");
        etOPW.clearFocus();
        etNPW.setText("");
        etNPW.clearFocus();
        SharedPreferences sp = getActivity().getSharedPreferences(SP, MODE_PRIVATE);
        String pw = sp.getString("pw", "1");
        if ((opw.equals(pw)) && ((npw.length() > 4 || npw.length() < 16))) {
            sp = getActivity().getSharedPreferences(SP, MODE_PRIVATE);
            ed = sp.edit();
            ed.putString("pw", npw).commit();
            StringRequest postRequest = new StringRequest(Request.Method.POST, UPDATE_PEMBELI,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // menampilkan respone
                            Log.d("Response POST", response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.e(TAG, "onErrorResponse: Error= " + error);
                            Log.e(TAG, "onErrorResponse: Error= " + error.getMessage());
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    // Menambahkan parameters post
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences sp = getActivity().getSharedPreferences(SP, MODE_PRIVATE);
                    params.put("nama", sp.getString("un", "1"));
                    params.put("pass", npw);
                    return params;
                }
            };
            AppController.getInstance().addToRequestQueue(postRequest);


           Toast.makeText(getActivity(), "Password successfully changed", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getActivity(), "Check Password", Toast.LENGTH_SHORT).show();
        }
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
