package irsyadhhs.cs.upi.edu.gopkl3;

public class ModelUser {

    public String name;
    public String email;
    public String UID;
    public String number;
    public String address;
    public String instanceId;


    public ModelUser() {
    }

    public ModelUser(String name, String email, String number, String address) {
        this.name = name;
        this.email = email;
        this.number = number;
        this.address = address;
    }

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }

    public void setEmail(String email){
        this.email = email;
    }
    public String getEmail(){
        return this.email;
    }

    public String getInstanceId() {
        return instanceId;
    }
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
