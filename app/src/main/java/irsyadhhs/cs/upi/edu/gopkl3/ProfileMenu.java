package irsyadhhs.cs.upi.edu.gopkl3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by HARVI on 5/11/2017.
 */

public class ProfileMenu extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        // change R.layout.yourlayoutfilename for each of your fragments
        setHasOptionsMenu(true);
        /*FragmentManager fm = getFragmentManager();
        MainFragment fragm = (MainFragment)fm.findFragmentById(R.id.main_fragment);
        fragm.otherList()*/
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Nearby");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.mSearch).setVisible(false);
        menu.findItem(R.id.mRefresh).setVisible(false);
        menu.findItem(R.id.mLogout).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void setListView(String d){
        Log.d("STRINGANJING", d);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.mRefresh) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
